Check_MK Enterprise Demo (free Enterprise for max 10 hosts)
===========================================================

[![Docker Repository on Quay](https://quay.io/repository/juliu_s/check-mk-enterprise-demo/status "Docker Repository on Quay")](https://quay.io/repository/juliu_s/check-mk-enterprise-demo)

[Based on this](https://github.com/tribe29/checkmk/tree/master/docker) (slightly adjusted) containerfile to build and run the Check_MK Enterprise demo version in a container.

1. Build container yourself

```shell-session
# docker build . -t foo/check-mk-enterprise-demo:1.6.0p6
```

Or build it yourself with a other version as build arg

```shell-session
# docker build . -t foo/check-mk-enterprise-demo:1.6.0p7 --build-arg CMK_VERSION=1.6.0p7
```

Or pull the latest version from Quay.io

```shell-session
# docker pull quay.io/juliu_s/check-mk-enterprise-demo
```

2. Run it

```shell-session
# docker container run -dit \
    --name monitoring \
    -p 8080:5000 \
    --ulimit nofile=1024 \
    -v "/datadir/on/containerhost/checkmk/:/omd/sites" \
    -v /etc/localtime:/etc/localtime \
    -e CMK_PASSWORD="cmkadmin" \
    --restart always \
    foo/check-mk-enterprise-demo:1.6.0p6
```

The checkmk web interface should now be reachable via http://localhost:8080/cmk/check_mk/

3. Check monitoring core

```shell-session
# docker exec -it monitoring su - cmk -c "omd config show"
```

4. Check the container logs

```shell-session
# docker logs -f monitoring
```

Links
-----
* [Checkmk website](https://checkmk.com/)
* [Checkmk container docs](https://mathias-kettner.com/cms_introduction_docker.html)
* [Create own Checkmk container](https://checkmk.com/cms_managing_docker.html#Creating%20your%20own%20container-images)
* [Checkmk RAW edition on Dockerhub](https://hub.docker.com/r/checkmk/check-mk-raw/)
* [Checkmk editions](https://checkmk.com/editions.html)

